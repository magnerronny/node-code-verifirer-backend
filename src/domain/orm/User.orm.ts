import { userEntity } from "../entities/User.entity";

import { LogError, LogSuccess } from "../../utils/logger";
import { StringDecoder } from "string_decoder";

//CRUD
/**
 * Method to obtain Users from collection "users" in mogo Server
 */
export const getAllUsers = async (): Promise<any[] | undefined> => {
  try {
    let userModel = userEntity();

    // Search all users
    return await userModel.find({isDelete: false})
  } catch (error) {
    LogError(`[ORM ERROR]: Getting All Users: ${error}`);
  }
}

// TODO:
// -Get User By ID
export const  getUserByID = async (id: string) : Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    // earch User BY ID
    return await userModel.findById(id)
  } catch (error) {
    LogError(`[ORM ERROR]: Getting User by: ${error}`);
  }
}


// - Delete User By ID

export const deleteUserByID = async (id: string): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    // delete user by id
    return await userModel.deleteOne({_id:id})
  } catch (error) {
    LogError(`[ORM ERROR]: Deleting User by ID: ${error}`);
  }
}


// Create new User
export const createUser = async (user: any): Promise<any | undefined> => {
  try {
    let userModel = userEntity();
    // Create / Insert new user
    return await userModel.create(user);
  } catch (error) {
    LogError(`[ORM ERROR]: Creating User: ${error}`);
  }
}

// - Update User By ID
export const updateUserByID = async (id: string, user: any): Promise<any | undefined> =>{
  try {
    let userModel = userEntity();
    //Update User
     return await userModel.findByIdAndUpdate(id, user);
  } catch (error) {
    LogError(`[ORM ERROR]: Updating User ${id}: ${error}`);
  }
}

// Get User By Email