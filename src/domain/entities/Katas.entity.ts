import mongoose from "mongoose";

export let katasEntity = () => {
  let Schema = mongoose.Schema;
  let katasSchema = new mongoose.Schema(
    {
      name: String,
      description: String,
      level: Number,
      user: {type: Schema.Types.ObjectId, ref: 'Users'},
      date:Date,
      valoration: [1,2,3,4,5],
      chances: Number
    
    }
  )

  return  mongoose.model('Katas', katasSchema);
}