/**
 * Root Router
 * Redirections to routers
 */
import express, {Request, Response} from 'express';
import helloRouter from './HelloRouter';
import { LogInfo } from '../utils/logger';
import goodbyeRouter from './GoodbyeRouter';
import usersRouter from './UserRouter';

//Server instance
let server = express();

//Router instance
let rootRouter = express.Router();

//Activate for requests to http://localhost:8000/api

// GET: http://localhost:8000/api/
rootRouter.get('/', (req: Request, res: Response) => {
  LogInfo( 'GET: http://localhost:8000/api/')
  res.status(200).json({"data":"Goodbye, world"});
})

// Redirections to Route & Controllers
server.use('/', rootRouter); //http://localhost:8000/api/
server.use('/hello', helloRouter); //http://localhost:8000/api//hello  -> HelloRouter
server.use('/goodbye', goodbyeRouter); //http://localhost:8000/api//goodbye  -> GoodbyeRouter
server.use('/users', usersRouter);

//Add more routes to the app

export default server;
