import express, {Express, Request, Response} from 'express';
import {GoodbyeController} from '../controller/GoodbyeController';
import { LogInfo } from '../utils/logger';

let goodbyeRouter = express.Router()


goodbyeRouter.route('/')
  .get( async(req: Request, res:Response) => {
    //Obtain a Query Param
    let name: any = req?.query?.name;
    LogInfo(`Query Param: ${name}`);
    //Controller Instance executed method
    const controller: GoodbyeController = new GoodbyeController();
    //Obtain Response
    const response = await controller.getMessage(name);
    //Send to the client the response
    let data = new Date(Date.now());
    return res.json({"message":response.message, "data": data.toString()});
  })
  
  export default goodbyeRouter;