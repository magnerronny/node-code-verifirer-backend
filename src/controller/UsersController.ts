import {Delete, Get, Post, Put, Query, Route, Tags} from "tsoa";
import { IUsersController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "..//utils/logger";

// ORM - Users Collection 
import { getAllUsers, getUserByID, deleteUserByID, createUser, updateUserByID} from "../domain/orm/User.orm";


@Route("/api/users")
@Tags("UserController")
export class UserController implements IUsersController {
  
  /**
   * End Point to retreive the Users in  the collection "users of db"
   * @param {string} id  Id of user to retreive (optional)
   * @returns all user foun by ID
   */
  @Get("/")
  public async getUsers(@Query()id?: string): Promise<any> {
    let response: any = '';
    if(id){
      LogSuccess(`[/api/users] Get User By ID: ${id}`);
      response = await getUserByID(id);
    }else{
      LogSuccess("[/api/users] Get All Users Request")
      response = await getAllUsers()
    }

    return response;
  }
/**
   * End Point to delete the Users in  the collection "users of db"
   * @param {string} id  Id of user to retreive (optional)
   * @returns message informing if deletion was correct
   */
  @Delete("/")
  public async deleteUser(@Query()id?: string): Promise<any> {
    let response: any = '';
    if(id){
      LogSuccess(`[/api/users] Delete User By ID: ${id}`);
      await deleteUserByID(id).then((r) => {
        response = {
          message : `User with id ${id} delete succesfuly`
        }
      });
    }else{
      LogWarning("[/api/users] Delete User Request without ID")
      response = {
        message: 'Please, provide an ID to remove from database'
      }
    }

    return response;
  }

  @Post("/")
  public async createUser(user: any): Promise<any> {
    let response: any = '';
    await createUser(user).then((r) => {
      LogSuccess(`[/api/users] Create User: ${user}`);
      response = {
        message: `User created: ${user.name}`
      }
    })
    return response;
  }

  @Put("/")
  public async updateUser(@Query()id: string, user: any): Promise<any> {
    let response: any = '';
    if(id){
      LogSuccess(`[/api/users] Update User By ID: ${id}`);
      await updateUserByID(id, user).then((r) => {
        response = {
          message : `User with id ${id} updated succesfuly`
        }
      });
    }else{
      LogWarning("[/api/users] Updated User Request without ID")
      response = {
        message: 'Please, provide an ID to update an existing user'
      }
    }

    return response;
  }

}