import { BasicResponse } from "../types";

export interface IHelloController {
  getMessage(name?:string): Promise<BasicResponse>
}

export interface IGoodbyeController {
  getMessage(name?:string): Promise<BasicResponse>
}

export interface IUsersController {
  // Read all users from database || get USer by  ID
  getUsers(id?:string): Promise<any>
    // Read all users from database || delete User by  ID
  deleteUser(id?:string): Promise<any>
  //Create new user
  createUser(user: any): Promise <any>
  //Update user
  updateUser(id:string, user:any): Promise <any>
}